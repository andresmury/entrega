package com.matrix.u90.myappleague.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Events  implements Serializable {

    @Expose
    @SerializedName("events")
    ArrayList<Event> list;

    public ArrayList<Event> getList() {
        return list;
    }

    public void setList(ArrayList<Event> list) {
        this.list = list;
    }
}
