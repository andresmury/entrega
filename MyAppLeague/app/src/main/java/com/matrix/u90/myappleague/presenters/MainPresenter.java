package com.matrix.u90.myappleague.presenters;

import com.matrix.u90.myappleague.interfaces.IMainView;
import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.repositories.Repository;

import java.io.IOException;
import java.util.ArrayList;

public class MainPresenter extends BasePresenter<IMainView> {

    private Repository repository;
    ArrayList<League>  listLigues;

    public void getOnLoadLeagues() throws IOException {
        repository = new Repository();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listLigues = repository.getListLeagues();
                    getView().loadAllLeagues(listLigues);
                }catch (IOException e){

                }

            }
        });
        thread.start();
    }

    public ArrayList<League> getListLigues() {
        return listLigues;
    }
}
