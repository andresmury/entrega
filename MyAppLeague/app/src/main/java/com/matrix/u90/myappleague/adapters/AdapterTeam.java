package com.matrix.u90.myappleague.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.matrix.u90.myappleague.R;
import com.matrix.u90.myappleague.interfaces.IDatosLigaView;
import com.matrix.u90.myappleague.models.Team;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterTeam extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Team> listaTeam;
    private IDatosLigaView iDatosLigaView;
    private Team team;

    public AdapterTeam(ArrayList<Team> listaTeam, IDatosLigaView iDatosLigaView) {
        this.listaTeam = listaTeam;
        this.iDatosLigaView = iDatosLigaView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_team,parent,false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) holder;
        team = listaTeam.get(position);
        customViewHolder.textViewStrStadium.setText(team.getStrStadium());
        customViewHolder.textViewStrTeam.setText(team.getStrTeam());
        customViewHolder.textViewStrStadiumLocation.setText(team.getStrStadiumLocation());

        if(team.getStrTeamBanner()!=null && !"".equalsIgnoreCase(team.getStrTeamBanner()))
        Picasso.get().load(team.getStrTeamBanner()).into(customViewHolder.imageViewTeamBanner);

        if(team.getStrTeamJersey()!=null && !"".equalsIgnoreCase(team.getStrTeamJersey()))
        Picasso.get().load(team.getStrTeamJersey()).into(customViewHolder.imageViewTeamJersey);

        if(team.getStrStadiumThumb()!=null && !"".equalsIgnoreCase(team.getStrStadiumThumb()))
        Picasso.get().load(team.getStrStadiumThumb()).into(customViewHolder.imageViewStadiumThumb);

    }

    @Override
    public int getItemCount() {
        return listaTeam.size();
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder{


        private TextView textViewStrTeam;
        private TextView textViewStrStadium;
        private ImageView imageViewTeamBanner;
        private ImageView imageViewTeamJersey;
        private ImageView imageViewStadiumThumb;
        private TextView textViewStrStadiumLocation;


        public CustomViewHolder(View itemView) {
            super(itemView);
            textViewStrTeam= itemView.findViewById(R.id.textViewStrTeam);
            textViewStrStadium  = itemView.findViewById(R.id.textViewStrStadium);
            imageViewTeamJersey = itemView.findViewById(R.id.imageViewTeamJersey);
            imageViewTeamBanner = itemView.findViewById(R.id.imageViewTeamBanner);
            imageViewStadiumThumb = itemView.findViewById(R.id.imageViewStadiumThumb);
            textViewStrStadiumLocation =  itemView.findViewById(R.id.textViewStrStadiumLocation);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if(pos != RecyclerView.NO_POSITION) {
                        iDatosLigaView.loadInfoTeam(listaTeam.get(pos));
                    }
                }
            });

        }
    }
}
