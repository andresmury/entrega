package com.matrix.u90.myappleague.presenters;

import com.matrix.u90.myappleague.interfaces.IDatosLigaView;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.repositories.Repository;


import java.io.IOException;
import java.util.ArrayList;

public class DatosLigaPresenter extends BasePresenter<IDatosLigaView> {

    private Repository repository;
    ArrayList<Team> listTeams;

    public void onLoadLisTeams(final String idLeague) throws IOException  {
        repository = new Repository();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listTeams = repository.getListTeam(idLeague);
                    getView().loadViewTeamsLigue(listTeams);
                }catch (IOException e){

                }

            }
        });
        thread.start();
    }
}
