package com.matrix.u90.myappleague.presenters;

import com.matrix.u90.myappleague.helper.ValidateInternet;
import com.matrix.u90.myappleague.views.IBaseView;



public class BasePresenter <T extends IBaseView> {
    private ValidateInternet validateInternet;
    private T view;



    public void inject(T view , ValidateInternet validateInternet) {
        this.view = view;
        this.validateInternet = validateInternet;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }


    public T getView() {
        return view;
    }


}
