package com.matrix.u90.myappleague.repositories;

import com.matrix.u90.myappleague.Services.IService;
import com.matrix.u90.myappleague.Services.ServicesFactory;
import com.matrix.u90.myappleague.models.Event;
import com.matrix.u90.myappleague.models.Events;
import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.models.Leagues;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.models.Teams;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Repository {

    private IService iService;

    public Repository(){
        ServicesFactory servicesFactory = new ServicesFactory();
        iService = (IService) servicesFactory.getInstanceServices(IService.class);
    }

    public ArrayList<Team> getListTeam(String idLiga) throws IOException {

        try{
            Call<Teams> call = iService.loadTeams(idLiga);
            Response<Teams> response =  call.execute();
            if(response.errorBody()!=null){
                throw defaultError();
            }else{
                return response.body().getListTeams();
            }

        }catch(IOException e){
            throw defaultError();
        }
    }

    public ArrayList<League> getListLeagues() throws IOException {

        try{
            Call<Leagues> call = iService.loadLeagues();
            Response<Leagues> response =  call.execute();

            if(response.errorBody()!=null){
                throw defaultError();
            }else{
                return response.body().getList();
            }
        }catch(IOException e){
            throw defaultError();
        }
    }


    public ArrayList<Event> getListEvents(String idTeam) throws IOException {

        try{
            Call<Events> call = iService.loadEventsTeam(idTeam);
            Response<Events> response =  call.execute();
            if(response.errorBody()!=null){
                throw defaultError();
            }else{
                return response.body().getList();
            }

        }catch(IOException e){
            throw defaultError();
        }
    }


    private IOException defaultError() {
        return new IOException("Ha ocurrido un error");
    }


}
