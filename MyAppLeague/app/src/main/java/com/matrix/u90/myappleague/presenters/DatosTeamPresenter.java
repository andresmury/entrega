package com.matrix.u90.myappleague.presenters;

import com.matrix.u90.myappleague.interfaces.IDatosTeamView;
import com.matrix.u90.myappleague.models.Event;
import com.matrix.u90.myappleague.models.Team;
import com.matrix.u90.myappleague.repositories.Repository;

import java.io.IOException;
import java.util.ArrayList;

public class DatosTeamPresenter  extends BasePresenter<IDatosTeamView> {
    private Repository repository;

    ArrayList<Event> listEvents;

    public void onLoadListEvents(final String idTeam) throws IOException {
        repository = new Repository();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listEvents = repository.getListEvents(idTeam);
                    getView().loadViewEventsTeam(listEvents);
                }catch (IOException e){

                }

            }
        });
        thread.start();
    }
}

