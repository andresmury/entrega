package com.matrix.u90.myappleague.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Event  implements Serializable {


    @SerializedName("idEvent")
    @Expose
    private String idEvent;

    @SerializedName("strEvent")
    @Expose
    private String strEvent;

    @SerializedName("strFilename")
    @Expose
    private String strFilename;

    @SerializedName("dateEvent")
    @Expose
    private String dateEvent;

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getStrEvent() {
        return strEvent;
    }

    public void setStrEvent(String strEvent) {
        this.strEvent = strEvent;
    }

    public String getStrFilename() {
        return strFilename;
    }

    public void setStrFilename(String strFilename) {
        this.strFilename = strFilename;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

}

