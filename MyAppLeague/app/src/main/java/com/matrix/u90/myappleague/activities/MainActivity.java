package com.matrix.u90.myappleague.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.matrix.u90.myappleague.R;
import com.matrix.u90.myappleague.interfaces.IMainView;
import com.matrix.u90.myappleague.models.League;
import com.matrix.u90.myappleague.presenters.MainPresenter;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity  extends BaseActivity<MainPresenter> implements IMainView {

    private ImageView imagenInicio;
    private ArrayList<League> leaguesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
        if(!getPresenter().getValidateInternet().isConnected()){
            showToast("No hay conexion a internet");
        }else {
            loadLeagues();
        }
    }



    private void loadLeagues(){
        try {
            getPresenter().getOnLoadLeagues();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadAllLeagues( ArrayList<League> listLeagues) {
        leaguesList =listLeagues;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            Intent intent = new Intent(MainActivity.this, DatosLigaActivity.class);
            intent.putExtra("listaLigas", leaguesList);
            startActivity(intent);
            finish();
            }
        });
    }
}


