package com.matrix.u90.myappleague.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Teams  implements Serializable {

    @Expose
    @SerializedName("teams")
     ArrayList<Team> listTeams;


    public ArrayList<Team> getListTeams() {
        return listTeams;
    }

    public void setListTeams(ArrayList<Team> listTeams) {
        this.listTeams = listTeams;
    }


}
