package com.matrix.u90.myappleague.views;

public interface IBaseView {

    void showAlertDialog();
    void showToast(String mensaje);
    void openLink(String link);
    void openEnlace(String enlace);
}
