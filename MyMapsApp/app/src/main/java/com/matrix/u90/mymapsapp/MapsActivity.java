package com.matrix.u90.mymapsapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.matrix.u90.mymapsapp.models.Cinema;
import com.matrix.u90.mymapsapp.models.Location;
import com.matrix.u90.mymapsapp.models.LocationsList;
import com.matrix.u90.mymapsapp.repositories.Repository;

import java.io.IOException;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ArrayList<LatLng> arrayPoints;
    Repository repository ;
    ArrayList<Cinema> listCinema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.style_map));

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
        loadCinemas();

    }

    private void createMarkers() {
        arrayPoints = new ArrayList<LatLng>();

        LatLng miCasa =new LatLng(6.2031813,-75.6032087);
        mMap.addMarker(new MarkerOptions().position(miCasa).title("Mi casa").icon(bitmapDescriptorFromVector(this,R.drawable.ic_location_on_black_24dp)));
        arrayPoints.add(miCasa);

        LatLng trabajo = new LatLng(6.2500324,-75.570322);
        mMap.addMarker(new MarkerOptions().position(trabajo).title("Mi Trabajo").icon(bitmapDescriptorFromVector(this,R.drawable.ic_subway_black_24dp)));
        arrayPoints.add(trabajo);

        LatLng cComercial = new LatLng(6.1964901,-75.5612781);
        mMap.addMarker(new MarkerOptions().position(cComercial).title("Centro comercial").icon(bitmapDescriptorFromVector(this,R.drawable.ic_local_activity_black_24dp)));
        arrayPoints.add(cComercial);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(miCasa,18));
        mMap.addPolyline(new PolylineOptions().add(miCasa,trabajo,cComercial,miCasa).width(4).color(Color.GREEN));

        centerPoints(arrayPoints);
    }

    private void createMarkersCinemas() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (listCinema!= null && listCinema.size()>0) {
                    loadAllCinemas(listCinema);
                }else{
                    Toast.makeText(MapsActivity.this,"ocurrio un error consultando los datos",Toast.LENGTH_LONG ).show();
                }
            }
        });



    }

    private void loadAllCinemas(final ArrayList<Cinema> listCinema) {
        arrayPoints = new ArrayList<LatLng>();

                for(Cinema tmp :listCinema ){
                    if(tmp.getLocationsList()!=null && tmp.getLocationsList().size()> 0){
                        for(LocationsList tmpLocation : tmp.getLocationsList()) {
                            LatLng latLngTmp = new LatLng(tmpLocation.getLocation().getCoordinates().get(1), tmpLocation.getLocation().getCoordinates().get(0));
                            mMap.addMarker(new MarkerOptions().position(latLngTmp).title(tmp.getName()+"-"+tmpLocation.getName()).icon(bitmapDescriptorFromVector(MapsActivity.this, R.drawable.ic_location_on_black_24dp)));
                            arrayPoints.add(latLngTmp);
                        }
                    }
                }
                calculateRoute(arrayPoints);
                centerPoints(arrayPoints);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context,vectorId);
        vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),vectorDrawable.getIntrinsicHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void centerPoints(ArrayList<LatLng> points){

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng tmp : points){
            builder.include(tmp);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,50);

        mMap.animateCamera(cameraUpdate);


    }

    private void loadCinemas(){
       if (isConnected()){
            createThreadLoadCinemas();

        }else{
            Toast.makeText(MapsActivity.this,"No tiene acceso a internet",Toast.LENGTH_LONG ).show();
        }
    }

    private void createThreadLoadCinemas() {
        repository = new Repository();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listCinema = repository.getCinemas();
                    createMarkersCinemas();

                }catch(IOException e){
                    e.printStackTrace();
                    Toast.makeText(MapsActivity.this,"Ocurrio un error consultando cinemas",Toast.LENGTH_LONG ).show();
                }
            }
        });
        thread.start();
    }


    private  boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) MapsActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return  networkInfo != null && networkInfo.isConnected();
    }

    private void calculateRoute (ArrayList<LatLng> points){
        Routing routing = new Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING).waypoints(points)
                .key(getString(R.string.google_maps_key))
                .optimize(true).withListener(routingListener)
                .build();
        routing.execute();
    }

    RoutingListener routingListener = new RoutingListener() {
        @Override
        public void onRoutingFailure(RouteException e) {
            Toast.makeText(MapsActivity.this,"Fallo el calculo de la ruta",Toast.LENGTH_LONG ).show();
        }

        @Override
        public void onRoutingStart() {

        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> arrayList, int index) {
            ArrayList polyLines = new ArrayList<>();
            for (Route tmpRoute :arrayList){
                PolylineOptions polylineOptions= new PolylineOptions();
                polylineOptions.color(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimary));
                polylineOptions.width(5);
                polylineOptions.addAll(tmpRoute.getPoints());

                Polyline polyline = mMap.addPolyline(polylineOptions);
                polyLines.add(polyline);
                int distance = tmpRoute.getDistanceValue();
                int duration = tmpRoute.getDurationValue();
                Toast.makeText(MapsActivity.this,"distancia:"+distance+"duracion:"+duration,Toast.LENGTH_LONG ).show();
                }
        }

        @Override
        public void onRoutingCancelled() {

        }
    };
}
